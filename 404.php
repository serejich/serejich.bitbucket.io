<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 Not Found</title>
    <style>

        * {
            margin: 0;
            padding: 0;
        }

        @import url('https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,600&subset=cyrillic');

        body {
            font-family: 'Montserrat', sans-serif; 
            overflow: hidden;
        }

        .content {
            height: 100vh;
            background-image: url('img/404.jpg');
            background-repeat: no-repeat;
            background-position: center bottom;
            background-size: cover;
            position: relative;
        }

        .overlay {
            background-color: rgba(0, 0, 0, .75);
            height: 100vh;
            padding: 0 2vw;
        }

        .error {
            padding: 22vh 0 0;
            font-size: 12em;
            font-weight: 600;
            color: #147bda;
            text-align: center;
            letter-spacing: 10px;
        }

        .find {
            font-size: 1.4em;
            color: #fff;
            text-align: center;
            padding: 0 0 6vh;
        }

        .back {
            border-radius: 30px;
            background-color: #7c93c7;
            color: #fff;
            padding: 16px 70px;
            text-transform: uppercase;
            font-size: 1em;
            border: none;
            outline: none;
            cursor: pointer;
            display: block;
            margin: 0 auto;
            transition: background-color .2s ease;
        }

        .back:hover {
            background-color: #346dec;
        }

        .back:active {
            background-color: #346dec;
        }

        .copyright {
            color: grey;
            width: 100%;
            text-align: center;
            position: absolute;
            bottom: 2vh;
            font-weight: 600;
            font-size: .9em;
        }

        @media screen and (max-width: 600px) {
            .content {
                background-position: bottom;
            }
            .error {
                font-size: 9em;
            }
        }

    </style>
</head>
<body>
    <section class="content">
        <div class="overlay">
            <h1 class="error">404</h1>
            <p class="find">Страница, которую вы ищете, не существует :(</p>
            <button onclick="window.history.back()" class="back">Назад</button>
            <p class="copyright">ГРЕНАДИН ФЕСТ<br><br>Copyright © 2019 Все права защищены</p>
        </div>
    </section>
</body>
</html>