<?php

    include('connect.php');

    echo '<h1 class="render-h1">Управление комментариями к новостям</h1>';

    $select_sql = "SELECT * FROM comments ORDER BY id ASC";
    $result = mysqli_query($conn, $select_sql);
    echo '<div class="comments">';
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<div class="comment">';
        $id_new = $row['id_new'];
        $select_new_sql = "SELECT article, href, img FROM news WHERE id = '$id_new'";
        $result_new = mysqli_query($conn, $select_new_sql);
        while ($row_new = mysqli_fetch_assoc($result_new)) {
            echo '<a class="news__a" href="news/'.$row_new["href"].'" title="'.$row_new["article"].'">
            <img class="news__a__img" src="img/news/'.$row_new["img"].'">
        </a>';
        }
        echo '<p class="comment-name">'.$row['name'].'</p>';
        echo '<p class="comment-msg">'.$row['message'].'</p>';
        echo '<button class="admin-btn del-comment-btn" value="'.$row["id"].'">Удалить</button>';
        echo '</div>';
    }
    echo '</div>';

    $conn->close();

?>