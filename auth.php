<?php
    session_start();

    if(isset($_SESSION['admin'])) {
        header('Location: admin');
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Авторизация</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="auth.css">
    <script src="dist/auth.js" type="module"></script>
</head>
<body>
    <div class="menu">
        <i class="fas fa-times close-m hidden"></i>
        <nav>
            <a class="border-bottom hidden" href="index">Главная</a>
            <a class="border-bottom hidden" href="participation">Подать заявку</a>
            <a class="border-bottom hidden" href="news">Новости</a>
        </nav>
    </div>
    <section class="overlay">
        <div class="menu-icon">
            <div class="menu-icon-hr"></div>
            <div class="menu-icon-hr"></div>
            <div class="menu-icon-hr"></div>
        </div>
        <span class="far fa-user-circle log"></span>
        <div class="container">
            <form>
                <i class="fas fa-times close"></i>
                <input type="text" name="login" id="login" placeholder="Логин" required>
                <input type="password" name="password" id="password" placeholder="Пароль" required>
                <input type="submit" value="Войти">
            </form>
        </div>
    </section>
</body>
</html>