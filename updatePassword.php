<?php

    include('connect.php');

    $id = $_POST['id'];
    $opassword = $_POST['opassword'];
    $npassword = $_POST['npassword'];
    if ($opassword == $npassword) {
        echo 'Новый пароль должен быть отличен от старого!';
        return;
    }
    $newpassword = password_hash($npassword, PASSWORD_DEFAULT);

    $select_sql = "SELECT password FROM admins WHERE id = '$id'";
    $result = mysqli_query($conn, $select_sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $opassworddb = $row['password'];
    }

    if (password_verify($opassword, $opassworddb)) {
        $update_sql = "UPDATE admins SET password = '$newpassword' WHERE id = '$id'";
        mysqli_query($conn, $update_sql);
        echo 'Пароль был успешно изменен.';
    } else {
        echo 'Текущий пароль неверен.';
    }

    $conn->close();

?>