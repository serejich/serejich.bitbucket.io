<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новости</title>
    <link rel="stylesheet" href="news.css">
</head>
<body>
    <section class="sidebar">
        <nav class="sidebar__nav">
            <a href="index" class="sidebar__nav__a">Главная</a>
            <a href="participation" class="sidebar__nav__a">Подать заявку</a>
            <a href="auth" class="sidebar__nav__a">Войти</a>
        </nav>
    </section>
    <section class="content">

        <section class="news">

            <?php

                include('connect.php');

                $select_sql = "SELECT * FROM news ORDER BY id ASC";
                $result = mysqli_query($conn, $select_sql);
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '<a href="news/'.$row['href'].'" class="news__a" title="'.$row["article"].'">
                        <img class="news__a__img" src="img/news/'.$row["img"].'">
                    </a>';
                }

                $conn->close();

            ?>

        </section>

    </section>

</body>
</html>