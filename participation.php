<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Заявка на участие в фестивале ГРЕНАДИН ФЕСТ</title>
    <link rel="stylesheet" href="participation.css">
    <script src="dist/participation.js" type="module"></script>
</head>
<body>
    <header>
        <img src="img/poster.jpg">
    </header>
    <main>
        <h1>Заявка на участие</h1>
        <select class="select" id="select">
        <option value="0" required>Выберите направление...</option>
        <option value="1" required>Участие в гастрономическом фестивале</option>
        <option value="2" required>Участие в развлекательной программе</option>
        </select>




        <div id="form-one">

            <h2>Участнику гастрономического фестиваля</h2>

            <form id="form1" name="form1"></form>

            <table class="table-info">
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Полное наименование компании:</span>
                        <input class="table-info-tr-td-input" name="company_name1" form="form1" type="text" required>
                    </td>
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Сайт:</span>
                        <input class="table-info-tr-td-input" name="company_site1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">ИНН:</span>
                        <input class="table-info-tr-td-input" name="inn1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">КПП:</span>
                        <input class="table-info-tr-td-input" name="kpp1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">БИК:</span>
                        <input class="table-info-tr-td-input" name="bik1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Юридический адрес:</span>
                        <input class="table-info-tr-td-input" name="ur_adres1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Почтовый/Фактический адрес:</span>
                        <input class="table-info-tr-td-input" name="po_adres1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Телефон:</span>
                        <input class="table-info-tr-td-input" name="tel1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Факс:</span>
                        <input class="table-info-tr-td-input" name="fax1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">E-mail:</span>
                        <input class="table-info-tr-td-input" name="em1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Ф.И.О. руководителя:</span>
                        <input class="table-info-tr-td-input" name="fio1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Ответственное лицо за участие в фестивале:</span>
                        <input class="table-info-tr-td-input" name="otv1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Ф.И.О.:</span>
                        <input class="table-info-tr-td-input" name="fio11" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">E-mail:</span>
                        <input class="table-info-tr-td-input" name="em11" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Должность:</span>
                        <input class="table-info-tr-td-input" name="dol1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Телефон:</span>
                        <input class="table-info-tr-td-input" name="tel11" form="form1" type="text">
                    </td>
                </tr>
            </table>

            <p class="ask">Просим зарегистрировать в качестве участника и предоставить в аренду торговую точку</p>

            <table class="table-info">
                <tr class="table-info-tr">
                    <td class="table-info-tr-td space-td" colspan="1">
                        <span class="table-info-tr-td-span">Вид продукции:</span>
                        <input class="table-info-tr-td-input" name="vid1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td space-td" colspan="1">
                            <span class="table-info-tr-td-span">Кол-во торговых точек:</span>
                            <input class="table-info-tr-td-input" name="kol1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td space-td" colspan="1">
                            <span class="table-info-tr-td-span">Ширина, м:</span>
                            <input class="table-info-tr-td-input" name="wid1" form="form1" type="text">
                    </td>
                    <td class="table-info-tr-td space-td" colspan="1">
                            <span class="table-info-tr-td-span">Длина, м:</span>
                            <input class="table-info-tr-td-input" name="hei1" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td space-td" colspan="4">
                        <span class="table-info-tr-td-span">Кол-во потребляемой электроэнергии (кВт/час):</span>
                        <input class="table-info-tr-td-input" name="kol11" form="form1" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td space-td" colspan="4">
                        <span class="table-info-tr-td-span">Примечания и особые пожелания:</span>
                        <input class="table-info-tr-td-input" name="prim1" form="form1" type="text">
                    </td>
                </tr>
            </table>
            <input class="submit" type="submit" form="form1" value="Отправить">
        

        </div>


        <div id="form-two">

        <h2>Участнику развлекательной программы</h2>

        <form id="form2" name="form2"></form>

            <table class="table-info">
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Полное наименование компании:</span>
                        <input class="table-info-tr-td-input" name="company_name2" form="form2" type="text" required>
                    </td>
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Сайт:</span>
                        <input class="table-info-tr-td-input" name="company_site2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">ИНН:</span>
                        <input class="table-info-tr-td-input" name="inn2" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">КПП:</span>
                        <input class="table-info-tr-td-input" name="kpp2" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">БИК:</span>
                        <input class="table-info-tr-td-input" name="bik2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Юридический адрес:</span>
                        <input class="table-info-tr-td-input" name="ur_adres2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Почтовый/Фактический адрес:</span>
                        <input class="table-info-tr-td-input" name="po_adres2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Телефон:</span>
                        <input class="table-info-tr-td-input" name="tel2" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Факс:</span>
                        <input class="table-info-tr-td-input" name="fax2" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">E-mail:</span>
                        <input class="table-info-tr-td-input" name="em2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Ф.И.О. руководителя:</span>
                        <input class="table-info-tr-td-input" name="fio2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="3">
                        <span class="table-info-tr-td-span">Ответственное лицо за участие в фестивале:</span>
                        <input class="table-info-tr-td-input" name="otv2" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Ф.И.О.:</span>
                        <input class="table-info-tr-td-input" name="fio22" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">E-mail:</span>
                        <input class="table-info-tr-td-input" name="em22" form="form2" type="text">
                    </td>
                </tr>
                <tr class="table-info-tr">
                    <td class="table-info-tr-td" colspan="2">
                        <span class="table-info-tr-td-span">Должность:</span>
                        <input class="table-info-tr-td-input" name="dol2" form="form2" type="text">
                    </td>
                    <td class="table-info-tr-td">
                        <span class="table-info-tr-td-span">Телефон:</span>
                        <input class="table-info-tr-td-input" name="tel22" form="form2" type="text">
                    </td>
                </tr>
            </table>
            <p class="ask">Просим зарегистрировать в качестве участника сценической программы</p>
            <div class="div2">
                <span class="span2">Описание программы: </span>
                <textarea name="opi2" cols="36" rows="6" form="form2"></textarea>
            </div>

            <div class="div2">
                <span class="span2">Время выступления: </span>
                <input type="text" name="vre2" form="form2">
            </div>

            <input class="submit" type="submit" form="form2" value="Отправить">
        
        </div>


    </main>
    <footer>
        <nav>
            <a href="index">Главная</a>
            <a href="news">Новости</a>
            <a href="auth">Войти</a>
        </nav>
    </footer>
</body>
</html>