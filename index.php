<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Гастрономический фестиваль ГРЕНАДИН</title>
	<link rel="stylesheet" href="style.css">
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(51813011, "init", {
			id:51813011,
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true,
			webvisor:true
	});
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/51813011" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">    
	<script src="dist/index.js" type="module"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
</head>
<body ontouchstart>
	<section class="preloader">
	  <div class="preloader__loader"></div>
	</section>
    <div class="menu">
        <i class="fas fa-times close-m hidden"></i>
        <nav>
            <a class="border-bottom hidden" href="participation">Подать заявку</a>
			<a class="border-bottom hidden" href="news">Новости</a>
			<a class="border-bottom hidden" href="auth">Войти</a>
        </nav>
    </div>
	<section class="wrapper">
		<section class="play">
			<div class="play__bg"></div>
			<div class="play__overlay"></div>
			<div class="play__inner">
				<nav class="play__inner__nav">
					<img src="img/logo.svg" class="logo">
					<a class="play__inner__nav__a" href="participation">Подать заявку</a>
					<a class="play__inner__nav__a" href="news">Новости</a>
					<a class="play__inner__nav__a" href="auth">Войти</a>
					<div class="menu-icon">
						<div class="menu-icon-hr"></div>
						<div class="menu-icon-hr"></div>
						<div class="menu-icon-hr"></div>
					</div>
				</nav>
				<h1 class="play__inner__h1">ГРЕНАДИН ФЕСТ</h1>
				<hr class="hr play__inner__hr">
				<p class="play__inner__p">
					<span class="play__inner__p__fest">Гастрономический фестиваль</span><br>
					<span class="play__inner__p__date">17-18 августа 2019 г.</span><br>
					<span class="play__inner__p__place">ПКиО «Сокольники», Москва</span>
				</p>
			</div>
		</section>
		<section class="extra">
			<div class="extra__strip"></div>
			<h1 class="extra__h1">О фестивале</h1>
			<hr class="hr extra__hr">
			<p class="extra__p">
			Гастрономический фестиваль «ГРЕНАДИН» состоится 17-18 августа 2019 г.
			 в рамках Первой Туристской недели регионов России<br><br>

			Место проведения: г. Москва, ПКиО «Сокольники», КВЦ «Сокольники», павильон № 2<br><br>
			Это праздник вкуснейшей еды, десертов и напитков,
			 познавательных мастер-классов, конкурсов и развлечений.

​			Фестиваль откроется костюмированным карнавальным шествием участников
			и посетителей мероприятия. На Фонтанной площади и прогулочных аллеях парка разместятся
			точки питания различных производителей и продавцов гастрономической продукции.
			Концертно-развлекательная программа на главной сцене Гастрономического фестиваля "ГРЕНАДИН"
			 создаст особую атмосферу праздника
			
			</p>
		</section>
		<section class="advantages">
			<h1 class="advantages__h1">У нас уютно</h1>
			<hr class="hr advantages__hr">
			<div class="advantages__container">
				<div class="advantages__container__advantage" style="background-color: #6ec7e0; z-index: 4;">
					<span class="fas fa-users icons_adv"></span>
					<p class="advantages__container__advantage__p">250 000 посетителей</p>
				</div>
				<div class="advantages__container__advantage" style="background-color: #cc90cc; right: 12px; z-index: 3">
					<span class="fas fa-calendar day icons_adv"></span>
					<p class="advantages__container__advantage__p">2 праздничных дня</p>
				</div>
				<div class="advantages__container__advantage" style="background-color: #f7765f; right: 24px; z-index: 2">
					<span class="fas fa-award icons_adv"></span>
					<p class="advantages__container__advantage__p">200 участников</p>
				</div>
				<div class="advantages__container__advantage" style="background-color: #99da40; right: 36px;">
					<span class="fas fa-concierge-bell icons_adv"></span>
					<p class="advantages__container__advantage__p">300 новых блюд</p>
				</div>
			</div>
			<h1 class="advantages__entry">Вход свободный!</h1>
			<video class="entry__video" muted autoplay loop controls>
				<source src="video/entry.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
			</video>
			<div class="presentation-div">
				<a target="_blank" href="pdf/presentation.pdf" class="presentation-a">Смотреть презентацию</a>
			</div>
			<div class="videos-youtube">
				<iframe src="https://www.youtube.com/embed/LmZID7E7zJo" frameborder="0" allowfullscreen>
				</iframe>
				<iframe src="https://www.youtube.com/embed/QQCNYmCiKBI" frameborder="0" allowfullscreen>
				</iframe>
			</div>
		</section>
		<section class="entertainment">
			<div class="entertainment__overlay">
				<div class="entertainment__overlay__container">
					<div class="entertainment__overlay__container__div">
						<span class="far fa-star ent-star"></span>
						<span class="entertainment__overlay__container__div__span">
							Во время проведения фестиваля проходят мастер-классы, интерактивные лекции, 
							конкурсы							
						</span>
					</div>
					<div class="entertainment__overlay__container__div">
						<span class="far fa-star ent-star"></span>
						<span class="entertainment__overlay__container__div__span">
							Выступление артистов, творческих коллективов, талантливых детей 
							и взрослых
						</span>
					</div>
					<div class="entertainment__overlay__container__div">
						<span class="far fa-star ent-star"></span>
						<span class="entertainment__overlay__container__div__span">
							Для участников фестиваля оборудовано современное
							продуктовое пространство							
						</span>
					</div>
					<div class="entertainment__overlay__container__div">
						<span class="far fa-star ent-star"></span>
						<span class="entertainment__overlay__container__div__span">
							Посетители смогут попробовать
							желаемый деликатес перед покупкой, упаковать
							гастрономические подарки, принять
							участие в розыгрыше призов и
							других новогодних мероприятиях фестиваля							
						</span>
					</div>
					<p class="entertainment__overlay__container__p">
						Станьте частью нашего гастрономического фестиваля!
					</p>
				</div>
			</div>
		</section>
		<section class="guests">
			<h1 class="guests__h1">Посетители и и участники фестиваля</h1>
			<hr class="hr guests__hr">
			<p class="guests__p">
				Фестиваль заинтересует широкую аудиторию москвичей и гостей столицы
				любого возраста и вкусовых предпочтений<br><br>
				Посетители парка станут участниками замечательного праздника еды и развлечений<br><br>
				Ведущие кулинары и бармены поделятся секретами вкуснейших рецептов  и способов подачи блюд и напитков<br><br><br><br>
				Участниками фестиваля станут организаторы игровых зон, мастер-классов для детей и взрослых, партнеры и спонсоры
				мероприятия, а также производители и продавцы:
			</p>
				<ul class="guests__ul">
					<li>мясной, молочной, рыбной, кондитерской, хлебо-булочной, диетической продукции;</li>
					<li>детского питания;</li>
					<li>эко продуктов (мед, орехи, злаки);</li>
					<li>ресторанной, уличной, вегетарианской, национальной кухни;</li>
					<li>снеков;</li>
					<li>сладостей;</li>
					<li>мороженого;</li>
					<li>золодных и горячих напитков;</li>
					<li>кухонных принадлежностей и бытовых приборов</li>
				</ul>
		</section>
		<section class="slider">
			<img src="img/slider-1.jpg" alt="">
			<img src="img/slider-2.jpg" alt="">
			<img src="img/slider-3.jpg" alt="">
			<img src="img/slider-4.jpg" alt="">
			<img src="img/slider-5.jpg" alt="">
			<img src="img/slider-6.jpg" alt="">
			<img src="img/slider-7.jpg" alt="">
		</section>
		<section class="classes">
			<h1 class="classes__h1">Мастер-классы</h1>
			<hr class="hr classes__hr">
			<div class="classes__grid">
				<div class="classes__grid__item">
					<img src="img/m1.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Ульзутуева Ирина Анатольевна</span><br><br><br>
							<span style="text-transform: uppercase;">Пэчворк</span>
						</div>
					</div>
				</div>
				<div class="classes__grid__item">
					<img src="img/m2.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Мальцева Зинаида Петровна</span><br><br><br>
							<span style="text-transform: uppercase;">Вышивка лентами</span>
						</div>
					</div>
				</div>
				<div class="classes__grid__item">
					<img src="img/m3.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Зарудная Валентина Анатольевна</span><br><br><br>
							<span style="text-transform: uppercase;">Оберег "Сороки"</span>
						</div>
					</div>
				</div>
				<div class="classes__grid__item">
					<img src="img/m4.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Филиппова Татьяна Михайловна</span><br><br><br>
							<span style="text-transform: uppercase;">Оригами</span>
						</div>
					</div>
				</div>
				<div class="classes__grid__item">
					<img src="img/m5.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Иванова Татьяна Алексеевна</span><br><br><br>
							<span style="text-transform: uppercase;">Плетение из газетной лозы</span>
						</div>
					</div>
				</div>
				<div class="classes__grid__item">
					<img src="img/m6.jpg" alt="" class="classes__grid__item__img">
					<div class="classes__grid__item__overlay">
						<div class="classes__grid__item__overlay__text">
							<span style="font-size: 1.2em;">Шишова Роза Васильевна</span><br><br><br>
							<span style="text-transform: uppercase;">Квиллинг</span>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="order">
			<h1 class="order__h1">Подать заявку на участие</h1>
			<div class="order__div order__a">
				<a target="_blank" href="participation">Продолжить</a>
			</div>
		</section>
		<section class="contacts">
			<div class="contacts__bg"></div>
			<div class="contacts__content">
				<h1 class="contacts__h1">Найти нас</h1>
				<hr class="hr contacts__hr">
				<form class="contact-form">
					<div class="form-group">
						<p class="group-p">Имя</p>
						<input type="text" placeholder="Введите имя" name="name" required>
					</div>
					<div class="form-group">
						<p class="group-p">E-mail</p>
						<input type="email" placeholder="Введите E-mail" name="email" required>
					</div>
					<div class="form-group">
						<p class="group-p">Тема</p>
						<select name="theme">
							<option value="1">Организация фестиваля</option>
							<option value="2">Участие в конкурсе</option>
							<option value="3">Прочее</option>
						</select>
					</div>
					<div class="form-group">
						<p class="group-p">Сообщение</p>
						<textarea name="message" rows="4" required placeholder="Введите сообщение"></textarea>
					</div>
					<input type="submit" value="Отправить">
				</form>
			</div>
		</section>
		<section class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2242.8946220074604!2d37.67439271560946!3d55.795067696199396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b535b7788c0391%3A0x1dbb7567c7495d08!2z0J_QsNGA0Log0KHQvtC60L7Qu9GM0L3QuNC60Lg!5e0!3m2!1sru!2sru!4v1546758531730" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</section>
		<section class="footer">
			<div class="footer__overlay">
				<div class="footer__overlay__container">
					<div class="footer__overlay__container__item">
						<h2 class="footer__overlay__container__item__h2">О нас</h2>
						<p class="footer__overlay__container__item__p">
						Фестиваль заинтересует широкую аудиторию москвичей
						 и гостей столицы любого возраста и вкусовых предпочтений
						</p>
						<a class="know-more">Узнать больше</a>
					</div>
					<div class="footer__overlay__container__item">
						<h2 class="footer__overlay__container__item__h2">Меню</h2>
						<nav class="footer__overlay__container__item__nav">
							<a href="participation">Подать заявку</a>
							<a href="news">Новости</a>
							<a href="auth">Войти</a>
						</nav>
					</div>
					<div class="footer__overlay__container__item">
						<h2 class="footer__overlay__container__item__h2">В социальных сетях</h2>
						<nav class="footer__overlay__container__item__nav">
							<a class="a-icons" href="https://vk.com/public147563837">
								<span class="fab fa-vk footer-icons"></span>
							</a>
							<a class="a-icons" href="https://www.instagram.com/grenadin.festival/">
								<span class="fab fa-instagram footer-icons"></span>
							</a>
							<a class="a-icons" href="http://www.facebook.com/groups/1756943721056200">
								<span class="fab fa-facebook-f footer-icons"></span>
							</a>
						</nav>
					</div>
				</div>
				<p class="footer__overlay__copyright">
					Copyright © 2019 Все права защищены
				</p>
			</div>
		</section>
	</section>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		$('.slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1000,
			centerMode: true,
			centerPadding: '40px',
			adaptiveHeight: true,
			arrows: false,
			variableWidth: true
		});
		});
	</script>
</body>
</html>