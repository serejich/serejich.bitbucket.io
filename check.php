<?php
    session_start();
    include('connect.php');

    $select_sql = "SELECT * FROM admins";
    $result = mysqli_query($conn, $select_sql);
    while ($row = mysqli_fetch_assoc($result)) {
        if (isset($_POST['login']) && $_POST['login'] == $row['login'] 
        && isset($_POST['password']) && password_verify($_POST['password'], $row['password'])) {
            $_SESSION['admin'] = $row['login'];
        }
    }

    // if ($_POST['login'] === 'Vishnevsky' && $_POST['password'] === '1') {
    //     $_SESSION['admin'] = $_POST['login'];
    // }

    if(isset($_SESSION['admin'])) {
        header('Location: admin');
    } else {
        echo 'Ошибка входа.';
    }

    $conn->close();
?>