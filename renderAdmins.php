<?php

    session_start();

    echo '<h1 class="render-h1">Управление администраторами</h1>';

    echo '<button class="admin-add-btn admin-btn">Добавить администратора</button>';

    echo '
    <form class="admin-add-form" enctype="multipart/form-data" action="addAdmin" method="POST">
    <label for="login">Логин</label>
    <input type="text" name="login" id="login" required><br><br>
    <label for="password">Пароль</label>
    <input type="password" name="password" id="password" required><br><br>
    <label for="password2">Повторите пароль</label>
    <input type="password" name="password2" id="password2" required><br><br>
    <label for="img">Изображение размером 60х60</label>
    <input type="file" name="img" id="img" required><br><br>
    <input class="admin-btn" type="submit" value="Добавить">
    </form>';

    include('connect.php');

    $select_sql = "SELECT * FROM admins ORDER BY id ASC";
    $result = mysqli_query($conn, $select_sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<div class="admin-div">';
        echo '<img class="admin-img" src="img/'.$row['img'].'">';
        if ($row['login'] == $_SESSION['admin']) {
            echo '<span class="admin-row admin-active">'.$row['login'].'</span>';
        } else {
            echo '<span class="admin-row">'.$row['login'].'</span>';
        }
        if ($row['login'] !== $_SESSION['admin'] && $row['login'] !== 'Vishnevsky') {
            echo '<button value="';
            echo $row['id'];
            echo '" class="admin-del-btn">Удалить администратора</button>';
        }
        if ($row['login'] == $_SESSION['admin']) {
            echo '<br><br>';
            echo '<button class="admin-ch-btn admin-btn">Изменить пароль</button>';
            echo '
            <form class="admin-ch-form">
            <label for="login">Введите текущий пароль</label>
            <input type="password" name="opassword" id="opassword" required><br><br>
            <label for="login">Введите новый пароль</label>
            <input type="password" name="npassword" id="pnassword" required><br><br>
            <input type="hidden" name="id" value='.$row['id'].'>';
            echo '<input class="admin-cha-btn admin-btn" type="submit" value="Изменить">';
            echo '</form>';
        }
        echo '</div>';
    }

    $conn->close();

?>