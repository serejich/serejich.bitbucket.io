import { makeXHR } from './xhr.js';

const PART = (function() {
    'use strict';

    let select = document.getElementById('select');
    let formOne = document.getElementById('form-one');
    let formTwo = document.getElementById('form-two');
    let renderForm = () => {
        select.addEventListener('change', (event) => {
            switch(event.target.value) {
                case '0':
                    formOne.style.display = 'none';
                    formTwo.style.display = 'none';
                    break;
                case '1':
                    formOne.style.display = 'block';
                    formTwo.style.display = 'none';
                    break;
                case '2':
                    formTwo.style.display = 'block';
                    formOne.style.display = 'none';
                    break;
                default:
                    break;
            }
        }, false);
    }

    return {
        renderForm: renderForm,
        form1: document.getElementById('form1'),
        form2: document.getElementById('form2')
    }
})();

window.addEventListener('load', () => {
    PART.renderForm();

    PART.form1.addEventListener('submit', (event) => {
        event.preventDefault();
        let data = new FormData(event.target);
        makeXHR('POST', 'orders-food-handler', true, data)
            .then(() => {
                alert('Заявка принята. Спасибо!')
            })
            .then(() => {
                event.target.reset();
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    });

    PART.form2.addEventListener('submit', (event) => {
        event.preventDefault();
        let data = new FormData(event.target);
        makeXHR('POST','orders-ent-handler', true, data)
            .then(() => {
                alert('Заявка принята. Спасибо!')
            })
            .then(() => {
                event.target.reset();
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    });

}, false);