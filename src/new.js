'use strict';

import { makeXHR } from './xhr.js';

const NEW = (function() {
    return {
        form: document.getElementsByClassName('new-form')[0],
        menuIcon: document.getElementsByClassName('menu-icon')[0],
		menuM: document.getElementsByClassName('menu')[0],
		closeM: document.getElementsByClassName('close-m')[0],
		delay: 200,
		hidden: document.getElementsByClassName('hidden')
    }
})();

window.addEventListener('load', () => {

    NEW.menuIcon.addEventListener('click', () => {
		NEW.menuM.style.height = '100%';
		NEW.menuM.getElementsByTagName('nav')[0].style.display = 'flex';
		setTimeout(() => {
			[].map.call(NEW.hidden, ((item, index) => {
				setTimeout(() => {
					item.style.visibility = 'visible';
					item.style.opacity = '1';
				}, index*NEW.delay)
			}));
		}, 1000);
	}, false);

	NEW.closeM.addEventListener('click', () => {
		NEW.menuM.getElementsByTagName('nav')[0].style.display = 'none';
		NEW.menuM.style.height = '0';
		for (let hiddenEl of NEW.hidden) {
			hiddenEl.style.visibility = 'hidden';
			hiddenEl.style.opacity = '0';
		}
	}, false);

    NEW.form.addEventListener('submit', (event) => {
        event.preventDefault();
        let data = new FormData(event.target);
        makeXHR('POST', '../addComment', true, data)
            .then(response => {
                alert(response);
            })
            .then(() => {
                event.target.reset();
            })
            .then(() => {
                window.location.reload();
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);
}, false);