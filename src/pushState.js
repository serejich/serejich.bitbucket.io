function JustPushState (URL) {
    'use strict';
    this.state = {};
    this.title = null;
    this.URL = URL;
    window.history.pushState(this.state, this.title, this.URL);
}
export { JustPushState };