function scrollTo(element) {
    'use strict';
    window.scrollTo({
        behavior: 'smooth',
        top: element.offsetTop
    });
}

export { scrollTo };