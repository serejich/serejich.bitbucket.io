'use strict';

import { FadeSlider } from './slider.js';
import { scrollTo } from './scrollto.js';
import { makeXHR } from './xhr.js';

const VISHNEVSKY = (function() {
	
	return {
		preloader: document.getElementsByClassName('preloader')[0],
		wrapper: document.getElementsByClassName('wrapper')[0],
		menu: document.getElementsByClassName('play__inner__nav')[0],
		knowMoreButton: document.getElementsByClassName('know-more')[0],
		form: document.getElementsByClassName('contact-form')[0],
		menuIcon: document.getElementsByClassName('menu-icon')[0],
		menuM: document.getElementsByClassName('menu')[0],
		closeM: document.getElementsByClassName('close-m')[0],
		delay: 200,
		hidden: document.getElementsByClassName('hidden')
	}
})();

window.addEventListener('load', () => {
	
		requestAnimationFrame(() => VISHNEVSKY.preloader.style.opacity = 0);
		VISHNEVSKY.preloader.addEventListener('transitionend', () => VISHNEVSKY.preloader.style.display = 'none');
		VISHNEVSKY.wrapper.style.display = 'block';
		let topSlider = new FadeSlider(2000);
		topSlider.nextImg(0);

	VISHNEVSKY.knowMoreButton.addEventListener('click', (event) => {
		event.preventDefault();
		scrollTo(document.getElementsByClassName('play')[0]);
	}, false);
	VISHNEVSKY.form.addEventListener('submit', (event) => {
		event.preventDefault();
		let data = new FormData(event.target);
		makeXHR('POST', 'contact', true, data)
			.then(response => {
				alert(response);
			})
			.then(() => {
				event.target.reset();
			})
			.catch(error => {
				console.log(`Error:  ${error.statusText}`);
			});
	}, false);
	VISHNEVSKY.menuIcon.addEventListener('click', () => {
		VISHNEVSKY.menuM.style.height = '100%';
		VISHNEVSKY.menuM.getElementsByTagName('nav')[0].style.display = 'flex';
		setTimeout(() => {
			[].map.call(VISHNEVSKY.hidden, ((item, index) => {
				setTimeout(() => {
					item.style.visibility = 'visible';
					item.style.opacity = '1';
				}, index*VISHNEVSKY.delay)
			}));
		}, 1000);
	}, false);

	VISHNEVSKY.closeM.addEventListener('click', () => {
		VISHNEVSKY.menuM.getElementsByTagName('nav')[0].style.display = 'none';
		VISHNEVSKY.menuM.style.height = '0';
		for (let hiddenEl of VISHNEVSKY.hidden) {
			hiddenEl.style.visibility = 'hidden';
			hiddenEl.style.opacity = '0';
		}
	}, false);
});

window.addEventListener('scroll', () => {
	if (window.pageYOffset > 500) {
		VISHNEVSKY.menu.style.backgroundColor = 'rgb(255, 143, 51)';
	} else {
		VISHNEVSKY.menu.style.backgroundColor = 'inherit';
	}
});