'use strict';

import { makeXHR } from './xhr.js';
import { JustPushState } from './pushState.js';

const ADMIN = (function() {
    return {
        ordersFoodBtn: document.getElementsByClassName('orders-food-a')[0],
        ordersEntBtn: document.getElementsByClassName('orders-ent-a')[0],
        newsBtn: document.getElementsByClassName('news-a')[0],
        commentsBtn: document.getElementsByClassName('comments-a')[0],
        adminsBtn: document.getElementsByClassName('admins-a')[0],
        contentDiv: document.getElementsByClassName('main__content')[0],
        self: {
            el: undefined
        }
    }
})();

window.addEventListener('load', () => {

    ADMIN.ordersFoodBtn.addEventListener('click', () => {
        makeXHR('GET', 'renderFoodOrders', true)
            .then(response => {
                if (response !== '<h1 class="render-h1">Управление заявками участников гастрономического фестиваля</h1><div class="orders"></div>') {
                    ADMIN.contentDiv.innerHTML = response;

                    let orderChBtns = document.getElementsByClassName('order-ch-btn');
                    for (let orderChBtn of orderChBtns) {
                        let table = orderChBtn.parentNode.getElementsByTagName('table')[0];
                        let inputs = table.getElementsByTagName('input');
                        for (let input of inputs) {
                            input.addEventListener('keyup', () => {
                                orderChBtn.removeAttribute('disabled');
                            }, false);
                        }
                        orderChBtn.addEventListener('click', (event) => {
                            let id = event.target.value;
                            let data = new FormData();
                            data.append('id', id);
                            for (let i = 0, j = inputs.length; i < j; i++) {
                                data.append(`input_${i}`, inputs[i].value);
                            }
                            makeXHR('POST', 'changeFoodOrder', true, data)
                                .then(response => {
                                    alert(response);
                                })
                                .then(() => {
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.log(`Error:  ${error.statusText}`);
                                });
                        }, false);
                    }
                } else {
                    ADMIN.contentDiv.innerText = 'Заявок участников гастрономического фестиваля нет.';
                }
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);

    ADMIN.ordersEntBtn.addEventListener('click', () => {
        makeXHR('GET', 'renderEntOrders', true)
            .then(response => {
                if (response !== '<h1 class="render-h1">Управление заявками участников развлекательной программы</h1><div class="orders"></div>') {
                    ADMIN.contentDiv.innerHTML = response;

                    let orderChBtns = document.getElementsByClassName('order-ch-btn');
                    for (let orderChBtn of orderChBtns) {
                        let table = orderChBtn.parentNode.getElementsByTagName('table')[0];
                        let inputs = table.getElementsByTagName('input');
                        for (let input of inputs) {
                            input.addEventListener('keyup', () => {
                                orderChBtn.removeAttribute('disabled');
                            }, false);
                        }
                        orderChBtn.addEventListener('click', (event) => {
                            let id = event.target.value;
                            let data = new FormData();
                            data.append('id', id);
                            for (let i = 0, j = inputs.length; i < j; i++) {
                                data.append(`input_${i}`, inputs[i].value);
                            }
                            makeXHR('POST', 'changeEntOrder', true, data)
                                .then(response => {
                                    alert(response);
                                })
                                .then(() => {
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.log(`Error:  ${error.statusText}`);
                                });
                        }, false);
                    }
                } else {
                    ADMIN.contentDiv.innerText = 'Заявок участников развлекательной программы нет.';
                }
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);

    ADMIN.newsBtn.addEventListener('click', () => {
        makeXHR('GET', 'renderNews', true)
            .then(response => {
                if (true) {
                    ADMIN.contentDiv.innerHTML = response;

                    let editArticleBtns = document.getElementsByClassName('edit-new-article');
                    for (let editArticleBtn of editArticleBtns) {
                        let article = editArticleBtn.parentNode.getElementsByClassName('new-article')[0];
                        let value = article.value;
                        editArticleBtn.parentNode.getElementsByClassName('new-article')[0]
                            .addEventListener('keyup', (event) => {
                                if (event.target.value !== value) {
                                    editArticleBtn.removeAttribute('disabled');
                                } else {
                                    editArticleBtn.setAttribute('disabled', '');
                                }
                            }, false);
                        editArticleBtn.addEventListener('click', (event) => {
                            let id = editArticleBtn.value;
                            let value = event.target.parentNode.getElementsByClassName('new-article')[0].value;
                            let data = new FormData();
                            data.append('id', id);
                            data.append('new-article', value);
                            makeXHR('POST', 'editNewArticle', true, data)
                                .then(response => {
                                    alert(response);
                                })
                                .then(() => {
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.log(`Error:  ${error.statusText}`);
                                });
                        }, false);
                    }

                    let editHrefBtns = document.getElementsByClassName('edit-new-href');
                    for (let editHrefBtn of editHrefBtns) {
                        let href = editHrefBtn.parentNode.getElementsByClassName('new-href')[0];
                        let value = href.value;
                        editHrefBtn.parentNode.getElementsByClassName('new-href')[0]
                            .addEventListener('keyup', (event) => {
                                if (event.target.value !== value) {
                                    editHrefBtn.removeAttribute('disabled');
                                } else {
                                    editHrefBtn.setAttribute('disabled', '');
                                }
                            }, false);
                        editHrefBtn.addEventListener('click', (event) => {
                            let id = editHrefBtn.value;
                            let value = event.target.parentNode.getElementsByClassName('new-href')[0].value;
                            let data = new FormData();
                            data.append('id', id);
                            data.append('new-href', value);
                            makeXHR('POST', 'editNewHref', true, data)
                                .then(response => {
                                    alert(response);
                                })
                                .then(() => {
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.log(`Error:  ${error.statusText}`);
                                });
                        }, false);
                    }

                    let editContentBtns = document.getElementsByClassName('edit-new-content');
                    for (let editContentBtn of editContentBtns) {
                        let content = editContentBtn.parentNode.getElementsByClassName('new-content')[0];
                        let value = content.value;
                        editContentBtn.parentNode.getElementsByClassName('new-content')[0]
                            .addEventListener('keyup', (event) => {
                                if (event.target.value !== value) {
                                    editContentBtn.removeAttribute('disabled');
                                } else {
                                    editContentBtn.setAttribute('disabled', '');
                                }
                            }, false);
                        editContentBtn.addEventListener('click', (event) => {
                            let id = editContentBtn.value;
                            let value = event.target.parentNode.getElementsByClassName('new-content')[0].value;
                            let data = new FormData();
                            data.append('id', id);
                            data.append('new-content', value);
                            makeXHR('POST', 'editNewContent', true, data)
                                .then(response => {
                                    alert(response);
                                })
                                .then(() => {
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.log(`Error:  ${error.statusText}`);
                                });
                        }, false);
                    }

                } else {
                    ADMIN.contentDiv.innerText = 'Новостей нет.';
                }
            })
            .then(() => {
                document.getElementsByClassName('news-add-btn')[0].addEventListener('click', (event) => {
                    event.preventDefault();
                    document.getElementsByClassName('news-add-form')[0].classList.toggle('show');
                }, false);
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);

    ADMIN.commentsBtn.addEventListener('click', () => {
        makeXHR('GET', 'renderComments', true)
            .then(response => {
                if (response !== '<h1 class="render-h1">Управление комментариями к новостям</h1><div class="comments"></div>') {
                    ADMIN.contentDiv.innerHTML = response;
                } else {
                    ADMIN.contentDiv.innerText = 'Комментариев к новостям нет.';
                }
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);

    ADMIN.adminsBtn.addEventListener('click', () => {
        makeXHR('GET', 'renderAdmins', true)
            .then(response => {
                ADMIN.contentDiv.innerHTML = response;
            })
            .then(() => {
                document.getElementsByClassName('admin-add-btn')[0].addEventListener('click', (event) => {
                    event.preventDefault();
                    document.getElementsByClassName('admin-add-form')[0].classList.toggle('show');
                }, false);
            })
            .catch(error => {
                console.log(`Error:  ${error.statusText}`);
            });
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('repeat-click')) {
            for (let nav of document.getElementsByClassName('nav')) {
                nav.classList.remove('blink');
            }
            if (!event.target.classList.contains('clicked')) {
                event.target.classList.add('clicked');
                if (ADMIN.self.el && ADMIN.self.el !== event.target) ADMIN.self.el.classList.remove('clicked');
                ADMIN.self.el = event.target;
            } else {
                ADMIN.self.el = event.target;
                ADMIN.contentDiv.classList.add('apply-shake');
                ADMIN.contentDiv.addEventListener('animationend', (event) => {
                    event.target.classList.remove('apply-shake');
                }, false);
            }
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('order-food-del-btn')) {
            let del = confirm('Удалить?');
            if (!del) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            makeXHR('POST', 'deleteFoodOrder', true, data)
                .then(() => {
                    makeXHR('GET', 'renderFoodOrders', true)
                    .then(response => {
                        if (response !== 
                            '<h1 class="render-h1">Управление заявками участников гастрономического фестиваля</h1><div class="orders"></div>') {
                            ADMIN.contentDiv.innerHTML = response;
                        } else {
                            ADMIN.contentDiv.innerText = 'Заявок участников гастрономического фестиваля нет.';
                        }
                    })
                    .catch(error => {
                        console.log(`Error:  ${error.statusText}`);
                    });
                })
                .catch(error => {
                    console.log(`Error:  ${error.statusText}`);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('order-ent-del-btn')) {
            let del = confirm('Удалить?');
            if (!del) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            makeXHR('POST', 'deleteEntOrder', true, data)
                .then(() => {
                    makeXHR('GET', 'renderEntOrders', true)
                    .then(response => {
                        if (response !== 
                            '<h1 class="render-h1">Управление заявками участников развлекательной программы</h1><div class="orders"></div>') {
                            ADMIN.contentDiv.innerHTML = response;
                        } else {
                            ADMIN.contentDiv.innerText = 'Заявок участников развлекательной программы нет.';
                        }
                    })
                    .catch(error => {
                        console.log(`Error:  ${error.statusText}`);
                    });
                })
                .catch(error => {
                    console.log(`Error:  ${error.statusText}`);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('order-food-mail-btn')) {
            let result = prompt('Введите E-mail');
            if (!result) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            data.append('mail', result);
            makeXHR('POST', 'mailFoodOrder', true, data)
                .then(response => { 
                    alert(response);
                })
                .catch(error => {
                    console.log(`Error:  ${error.statusText}`);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('order-ent-mail-btn')) {
            let result = prompt('Введите E-mail');
            if (!result) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            data.append('mail', result);
            makeXHR('POST', 'mailEntOrder', true, data)
                .then(response => { 
                    alert(response);
                })
                .catch(error => {
                    console.log(`Error:  ${error.statusText}`);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('admin-del-btn')) {
            let del = confirm('Удалить?');
            if (!del) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            makeXHR('POST', 'deleteAdmin', true, data)
                .then(() => {
                    makeXHR('GET', 'renderAdmins', true)
                    .then(response => {
                        ADMIN.contentDiv.innerHTML = response;
                    })
                    .then(() => {
                        document.getElementsByClassName('admin-add-btn')[0].addEventListener('click', (event) => {
                            event.preventDefault();
                            document.getElementsByClassName('admin-add-form')[0].classList.toggle('show');
                        }, false);
                    })
                    .catch(error => {
                        console.log(`Error:  ${error.statusText}`);
                    });
                })
                .catch(error => {
                    console.log(`Error:  ${error.statusText}`);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('admin-ch-btn')) {
            document.getElementsByClassName('admin-ch-form')[0].classList.toggle('show');
            document.getElementsByClassName('admin-ch-form')[0].addEventListener('submit', (event) => {
                event.preventDefault();
                let data = new FormData(event.target);
                makeXHR('POST', 'updatePassword', true, data)
                    .then(response => {
                        alert(response);
                        if (response === 'Пароль был успешно изменен.') window.location.href = 'exit';
                    })
                    .catch(error => {
                        console.log(`Error:  ${error.statusText}`);
                    });
            }, false);
        }
    });
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('del-new')) {
            let del = confirm('Удалить?');
            if (!del) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            makeXHR('POST', 'deleteNew', true, data)
                .then(() => {
                    makeXHR('GET', 'renderNews', true)
                        .then(response => {
                            ADMIN.contentDiv.innerHTML = response;
                        })
                        .then(() => {
                            document.getElementsByClassName('news-add-btn')[0].addEventListener('click', (event) => {
                                event.preventDefault();
                                document.getElementsByClassName('news-add-form')[0].classList.toggle('show');
                            }, false);
                        })
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }, false);
    document.addEventListener('click', (event) => {
        if (event.target.classList.contains('del-comment-btn')) {
            let del = confirm('Удалить?');
            if (!del) return;
            let id = event.target.value;
            let data = new FormData();
            data.append('id', id);
            makeXHR('POST', 'deleteComment', true, data)
                .then(() => {
                    makeXHR('GET', 'renderComments', true)
                        .then(response => {
                            if (response !== '<h1 class="render-h1">Управление комментариями к новостям</h1><div class="comments"></div>') {
                                ADMIN.contentDiv.innerHTML = response;
                            } else {
                                ADMIN.contentDiv.innerText = 'Комментариев к новостям нет.';
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }, false);
}, false);