function makeXHR(method, url, async, data, type) {
    'use strict';
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url, async);
        if (type) xhr.responseType = type;
        xhr.addEventListener('load', () => {
            if (xhr.status === 200) {
                resolve(xhr.response);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        }, false);
        xhr.addEventListener('error', () => {
            reject({
                status: xhr.status,
                statusText: xhr.statusText
            });
        }, false);
        if (data) {
            xhr.send(data);
        } else {
            xhr.send(null);
        }
    });
};

export { makeXHR };