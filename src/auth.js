'use strict';

import { makeXHR } from './xhr.js';

const AUTH = (function() {
    return {
        log: document.getElementsByClassName('log')[0],
        container: document.getElementsByClassName('container')[0],
        close: document.getElementsByClassName('close')[0],
        overlay: document.getElementsByClassName('overlay')[0],
        form: document.getElementsByTagName('form')[0],
        login: document.getElementById('login'),
        password: document.getElementById('password'),
        menuIcon: document.getElementsByClassName('menu-icon')[0],
        menu: document.getElementsByClassName('menu')[0],
        closeM: document.getElementsByClassName('close-m')[0],
        delay: 200,
        hidden: document.getElementsByClassName('hidden')
    }
})();

window.addEventListener('load', () => {
    AUTH.log.addEventListener('click', (event) => {
        event.target.classList.add('fadeOut');
        setTimeout(() => {
            AUTH.container.classList.add('scale--on'); 
        }, 500);
    }, false);
    AUTH.close.addEventListener('click', (event) => {
        event.target.parentNode.parentNode.classList.remove('scale--on');
        setTimeout(() => {
            AUTH.log.classList.remove('fadeOut');
        }, 700);
    }, false);
}, false);

AUTH.form.addEventListener('submit', (event) => {
    event.preventDefault();
    let data = new FormData(event.target);
    makeXHR('POST', './check', true, data)
        .then(response => {
            if (response == 'Ошибка входа.') {
                alert(response);
            } else {
                window.location.href = 'admin';
            }
        })
        .then(() => {
            event.target.reset();
        })
        .catch(error => {
            console.log(`Error:  ${error.statusText}`);
        });
}, false);

AUTH.menuIcon.addEventListener('click', () => {
    AUTH.menu.style.height = '100%';
    AUTH.menu.getElementsByTagName('nav')[0].style.display = 'flex';
    setTimeout(() => {
        [].map.call(AUTH.hidden, ((item, index) => {
            setTimeout(() => {
                item.style.visibility = 'visible';
                item.style.opacity = '1';
            }, index*AUTH.delay)
        }));
    }, 1000);
}, false);

AUTH.closeM.addEventListener('click', () => {
    AUTH.menu.getElementsByTagName('nav')[0].style.display = 'none';
    AUTH.menu.style.height = '0';
    for (let hiddenEl of AUTH.hidden) {
        hiddenEl.style.visibility = 'hidden';
        hiddenEl.style.opacity = '0';
    }
}, false);