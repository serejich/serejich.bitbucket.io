function FadeSlider(speed) {
    'use strict';
    this.element = document.getElementsByClassName('play__bg')[0];
    this.URL = getComputedStyle(this.element).backgroundImage;
    this.IMAGES = ['url(./img/1.jpg)', 'url(./img/2.jpg)', 'url(./img/3.jpg)', 'url(./img/4.jpg)'];
    this.speed = speed;
    this.nextImg = (id) => {
        setInterval(() => {
            this.element.style.backgroundImage = this.IMAGES[id];
            id++;
            if (id > this.IMAGES.length-1) id = 0;
        }, this.speed);
    };
};

export { FadeSlider };