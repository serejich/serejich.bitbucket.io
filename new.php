<?php

    include('connect.php');
    
    if (isset($_GET['href'])) {
        $href = substr($_GET['href'], 0, -4);

        $select_sql = "SELECT * FROM news WHERE href = '$href'";
        $result = mysqli_query($conn, $select_sql);

        if (mysqli_num_rows($result) === 0) {
            header('HTTP/1.0 404 Not Found', true, 404);
            header('Location: /404');
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
                $article = $row['article'];
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $article?></title>
    <link rel="stylesheet" href="../new.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">    
    <script src="../dist/new.js" type="module"></script>
</head>
<body>
    <header>
    <div class="menu">
        <i class="fas fa-times close-m hidden"></i>
        <nav>
            <a class="border-bottom hidden" href="../index">Главная</a>
            <a class="border-bottom hidden" href="../participation">Подать заявку</a>
            <a class="border-bottom hidden" href="../news">Новости</a>
        </nav>
    </div>
        <nav class="nav">
			<a class="nav__a" href="../participation">Подать заявку</a>
			<a class="nav__a" href="../news">Новости</a>
			<a class="nav__a" href="../auth">Войти</a>
			<div class="menu-icon">
				<div class="menu-icon-hr"></div>
				<div class="menu-icon-hr"></div>
				<div class="menu-icon-hr"></div>
			</div>
		</nav>
    </header>
    <main>
    <?php

        $select_sql = "SELECT * FROM news WHERE href = '$href'";
        $result = mysqli_query($conn, $select_sql);

        while ($row = mysqli_fetch_assoc($result)) {
            echo '<div class="new">';
            echo '<img class="new-img" src="../img/news/'.$row['img'].'">';
            echo '<p class="new-article">'.$row['article'].'</p>';
            echo '<p class="new-content">'.$row['content'].'</p>';
            echo '</div>';

            $id = $row['id'];


        echo '<hr class="hr new-hr">';

        echo '<form class="new-form">
            <label for="name">Имя</label>
            <input required type="text" name="name" id="name">
            <label for="message">Сообщение</label>
            <textarea rows="6" required id="message" name="message"></textarea>
            <input type="hidden" name="id" value="'.$row['id'].'">
            <input type="submit" value="Комментировать">
            </form>';

        }


        $select_comments = "SELECT * FROM comments WHERE id_new='$id'";
        $result_comments = mysqli_query($conn, $select_comments);
        echo '<div class="comments">';
        while ($row = mysqli_fetch_assoc($result_comments))  {
            echo '<div class="comment">';
            echo '<p class="comment-name">'.$row["name"].'</p>';
            echo '<p class="comment-message">'.$row["message"].'</p>';
            echo '</div>';
        }
        echo '</div>';

        $conn->close();

    ?>
    </main>
</body>
</html>