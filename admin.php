<?php

    session_start();
    if (!isset($_SESSION['admin'])) {
        header('Location: auth');
    } else {
        $login = $_SESSION['admin'];
    }


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Панель администратора</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="admin.css">
    <script src="dist/admin.js" type="module"></script>
</head>
<body>
    <section class="sidebar">
        <ul class="sidebar__ul">
            <a class="sidebar__ul__a orders-food-a">
                <span class="blink nav repeat-click sidebar__ul__a__span fas fa-utensils"></span>
            </a>
            <a class="sidebar__ul__a orders-ent-a">
                <span class="blink nav repeat-click sidebar__ul__a__span fas fa-mask"></span>
            </a>
            <a class="nav sidebar__ul__a news-a">
                <span class="blink nav repeat-click sidebar__ul__a__span far fa-newspaper"></span>
            </a>
            <a class="nav sidebar__ul__a comments-a">
                <span class="blink nav repeat-click sidebar__ul__a__span far fa-comments"></span>
            </a>
            <a class="nav sidebar__ul__a admins-a">
                <span class="blink nav repeat-click sidebar__ul__a__span fas fa-user-friends"></span>
            </a>
            <a class="sidebar__ul__a exit-a" href="exit">
                <span class="sidebar__ul__a__span fas fa-door-open"></span>
            </a>
        </ul>
    </section>
    <section class="main">
        <div class="main__header">
            <div class="main__header__login">
                <span><?php echo $login?></span>
                <?php
                    include('connect.php');
                    $result = mysqli_query($conn, "SELECT img FROM admins WHERE login = '$login'");
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<img src="img/'.$row['img'].'">';
                    }
                ?>
            </div>
        </div>
        <div class="main__content">
            <h1 class="main__content__h1">Нужны какие-то изменения?</h1>
        </div>
    </section>
</body>
</html>