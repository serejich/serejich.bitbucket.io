const path = require('path');

module.exports = {
  entry: {
    'admin': ['@babel/polyfill', './src/admin.js'],
    'auth': ['@babel/polyfill', './src/auth.js'],
    'index': ['@babel/polyfill', './src/index.js'],
    'new': ['@babel/polyfill', './src/new.js'],
    'participation': ['@babel/polyfill', './src/participation.js'],
    'pushState': ['@babel/polyfill', './src/pushState.js'],
    'scrollto': ['@babel/polyfill', './src/scrollto.js'],
    'slider': ['@babel/polyfill', './src/slider.js'],
    'xhr': ['@babel/polyfill', './src/xhr.js']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            include: [
              path.resolve(__dirname, './src/')
            ],
            use: {
                loader: 'babel-loader'
           }
        }
     ]
 }
};