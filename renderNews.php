<?php

    include('connect.php');

    echo '<h1 class="render-h1">Управление новостями</h1>';

    echo '<button class="news-add-btn admin-btn">Добавить новость</button>';

    echo '
    <form class="news-add-form" enctype="multipart/form-data" action="addNew" method="POST">
    <label for="article">Заголовок</label>
    <input type="text" name="article" id="article" required><br><br>
    <label for="href">Быстрая ссылка</label>
    <input type="text" name="href" id="href" required><br><br>
    <label for="content">Содержание</label>
    <textarea id="content" name="content" required></textarea><br><br>
    <input type="file" name="img" id="img" required><br><br>
    <input class="admin-btn" type="submit" value="Добавить">
    </form>';

    $select_sql = "SELECT * FROM news ORDER BY id ASC";
    $result = mysqli_query($conn, $select_sql);
    echo '<div class="news">';
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<div class="new">';
        echo '<img class="new-img" src="img/news/'.$row['img'].'">';
        echo '<label for="new-article">Заголовок</label>';
        echo '<input type="text" class="new-article" value="'.$row['article'].'">';
        echo '<button value="'.$row["id"].'" disabled class="admin-btn edit-new-article">Сохранить</button>';
        echo '<label for="new-href">Быстрая ссылка</label>';
        echo '<input type="text" class="new-href" value="'.$row['href'].'">';
        echo '<button value="'.$row["id"].'" disabled class="admin-btn edit-new-href">Сохранить</button>';
        echo '<label for="new-content">Содержание</label>';
        echo '<textarea class="new-content">'.$row['content'].'</textarea>';
        echo '<button value="'.$row["id"].'" disabled class="admin-btn edit-new-content">Сохранить</button><br><br>';
        echo '<button class="admin-btn del-new" value="'.$row["id"].'">Удалить</button>';
        echo '</div>';
    }
    echo '</div>';

    $conn->close();

?>