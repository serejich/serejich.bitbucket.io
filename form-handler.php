<?php

$company_name = $_POST['company_name'];
$company_site = $_POST['company_site'];
$inn = $_POST['inn'];
$kpp = $_POST['kpp'];
$bik = $_POST['bik'];
$ur_adres = $_POST['ur_adres'];
$po_adres = $_POST['po_adres'];
$tel1 = $_POST['tel1'];
$fax1 = $_POST['fax1'];
$em1 = $_POST['em1'];
$fio1 = $_POST['fio1'];
$otv = $_POST['otv'];
$fio2 = $_POST['fio2'];
$em2 = $_POST['em2'];
$dol2 = $_POST['dol2'];
$tel2 = $_POST['tel2'];
$vid = $_POST['vid'];
$kol = $_POST['kol'];
$prim = $_POST['prim'];

// $message = 'Полное наименование компании: '.$company_name.'\n
// Сайт компании: '.$company_site.'\n
// ИНН: '.$inn.'\n
// БИК: '.$bik.'\n
// Юридический адрес: '.$ur_adres.'\n
// Почтовый\/Фактический адрес'.$ur_adres.'\n
// Телефон: '.$tel1.'\n
// Факс: '.$fax1.'\n
// E-mail: '.$em1.'\n
// ФИО: '.$fio1.'\n
// Ответственное лицо: '.$otv.'\n
// ФИО: '.$fio2.'\n
// E-mail: '.$em2.'\n
// Должность: '.$dol2.'\n
// Телефон: '.$tel2.'\n
// Вид деятельности: '.$vid.'\n
// Кол-во потребляемой энергии'.$kol.'\n
// Примечания: '.$prim;

$table_order = '<table class="table-info">
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="2">
        <span class="table-info-tr-td-span">Полное наименование компании:</span>
        '.$company_name.'
    </td>
    <td class="table-info-tr-td" colspan="2">
        <span class="table-info-tr-td-span">Сайт:</span>
        '.$company_site.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">ИНН:</span>
        '.$inn.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">КПП:</span>
        '.$kpp.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">БИК:</span>
        '.$bik.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="3">
        <span class="table-info-tr-td-span">Юридический адрес:</span>
        '.$ur_adres.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="3">
        <span class="table-info-tr-td-span">Почтовый/Фактический адрес:</span>
        '.$po_adres.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">Телефон:</span>
        '.$tel1.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">Факс:</span>
        '.$fax1.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">E-mail:</span>
        '.$em1.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="3">
        <span class="table-info-tr-td-span">Ф.И.О. руководителя:</span>
        '.$fio1.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="3">
        <span class="table-info-tr-td-span">Ответственное лицо за участие в фестивале:</span>
        '.$otv.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="2">
        <span class="table-info-tr-td-span">Ф.И.О.:</span>
        '.$fio2.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">E-mail:</span>
        '.$fio2.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td" colspan="2">
        <span class="table-info-tr-td-span">Должность:</span>
        '.$dol2.'
    </td>
    <td class="table-info-tr-td">
        <span class="table-info-tr-td-span">Телефон:</span>
        '.$tel2.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td space-td" colspan="3">
        <span class="table-info-tr-td-span">Вид продукции:</span>
        '.$vid.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td space-td" colspan="3">
        <span class="table-info-tr-td-span">Кол-во потребляемой электроэнергии (кВт/час):</span>
        '.$kol.'
    </td>
</tr>
<tr class="table-info-tr">
    <td class="table-info-tr-td space-td" colspan="3">
        <span class="table-info-tr-td-span">Примечания и особые пожелания:</span>
        '.$prim.'
    </td>
</tr>
</table>';

include('connect.php');

$insert_sql = "INSERT INTO orders(id, company_name, table_order) VALUES('', '$company_name', '$table_order')";
mysqli_query($conn, $insert_sql);

$conn->close();


?>